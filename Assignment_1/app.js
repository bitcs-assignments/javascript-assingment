const form = document.getElementById('form')

form.addEventListener('submit', event => {
    event.preventDefault()
    error.style.color = 'red'

    let pass = document.getElementById('pass').value
    if (pass.length < 6) {
        error.textContent = 'Password Length should be more than 6!'
        return
    } else if (pass.search(/[a-z]/) < 0) {
        error.textContent = 'Password should contain a lower case letter!'
        return
    } else if (pass.search(/[A-Z]/) < 0) {
        error.textContent = 'Password should contain an upper case letter!'
        return
    } else  if (pass.search(/[0-9]/) < 0) {
        error.textContent = 'Password should contain a digit!'
        return
    }

    let cbCount = 0, cbArray = []
    for (let i = 1; i < 5; i++) {
        const val = `p${i}`
        if (document.getElementById(val).checked) {
            cbCount = cbCount+1
            cbArray.push(document.getElementById(val).value)
        }
    }

    if (cbCount < 2) {
        error.textContent = 'Select atleast 2 permissions'
        return
    }

    error.textContent = ''

    let email = document.getElementById('email').value
    
    let role = 'Admin'
    if (document.getElementById('user').checked) role = 'User'
    
    let sex = document.getElementById('sex').value

    form.innerHTML = `<h2>Email: ${email}</h2>
                    <h2>Password: ${pass}<h2>
                    <h2>Sex: ${sex}</h2>
                    <h2>Role: ${role}</h2>
                    <h2>Permissions: ${cbArray}</h2>
                    <button>Confirm</button>`                           
})

                                                          